package com.sanish.repository.com.sanish.repository;

import com.sanish.model.Speaker;

import java.util.List;

public interface SpeakerRepository {
    List<Speaker> findAll();
}

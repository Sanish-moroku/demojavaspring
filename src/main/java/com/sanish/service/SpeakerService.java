package com.sanish.service;

import com.sanish.model.Speaker;

import java.util.List;

public interface SpeakerService {
    List<Speaker> findAll();
}

package com.sanish.service;

import com.sanish.model.Speaker;
import com.sanish.repository.HibernateSpeakerRepositoryImpl;
import com.sanish.repository.com.sanish.repository.SpeakerRepository;

import java.util.List;

public class SpeakerServiceImpl implements SpeakerService {
    private SpeakerRepository repository = new HibernateSpeakerRepositoryImpl();

    public List<Speaker> findAll(){
        return repository.findAll();
    }
}

package com.sanish.repository;

import com.sanish.model.Speaker;
import com.sanish.repository.com.sanish.repository.SpeakerRepository;

import java.util.ArrayList;
import java.util.List;

public class HibernateSpeakerRepositoryImpl implements SpeakerRepository {

    public List<Speaker> findAll(){
        List<Speaker> speakerList = new ArrayList<Speaker>();
        Speaker speaker = new Speaker();
        speaker.setFirstName("Sanish");
        speaker.setLastName("KS");

        speakerList.add(speaker);
        return speakerList;
    }


}
